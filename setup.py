#!/usr/bin/env python3
# coding=utf-8
# import
from setuptools import setup, find_packages
# for version norm, see : https://www.python.org/dev/peps/pep-0440/#post-releases
# define setup parameters
setup(
    name="NeuralNetworkCLI",
    version="0.0.1",
    description="Neural network CLI",
    packages=find_packages(),
    install_requires=["requests","neuralnetworkcommon"],
    data_files=[("shell/neuralnetworkcli", ["neuralnetworkcli/help/neuralnetworkcli.txt"])],
    classifiers=[
        'Programming Language :: Python :: 3',
    ],
)
