#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.cli.cli import parser, ActionType as CommonActionType, create, readAll, readSingle, update, delete, patch, summarize, execute
from neuralnetworkcommon.service.service import ResourcePathType, specificWorkspaceUrl, globalWorkspaceUrl, globalPerceptronUrl, specificPerceptronUrl, perceptronSummaryUrl, perceptronExecutionUrl, globalLayerUrl, specificLayerUrl, layerSummaryUrl
from os import sep, environ
from os.path import join, realpath
from sys import argv
from pythoncommontools.service.httpSymbol import getServerUrl
from pythoncommontools.cli.cli import main, raiseArgumentsException
from enum import Enum, unique
# contants
currentDirectory = realpath(__file__).rsplit(sep, 1)[0]
extraHelpFile=join(currentDirectory,"help","neuralnetworkcli.txt")
# load configuration
host = environ.get("NEURALNETWORK_CLI_HOST")
port = int(environ.get("NEURALNETWORK_SERVICE_PORT"))
endpoint = environ.get("NEURALNETWORK_SERVICE_ENDPOINT")
serverUrl=getServerUrl(host,port)
# global URL
serverGlobalWorkspaceUrl = serverUrl+globalWorkspaceUrl(endpoint)
serverGlobalPerceptronUrl = serverUrl+globalPerceptronUrl(endpoint)
serverGlobalLayerUrl = serverUrl+globalLayerUrl(endpoint)
# CLI parameter
@unique
class ShortCode(Enum):
    CURRENT_DIMENSION="-c"
    DEPTH_INDEX="-d"
    PREVIOUS_DIMENSION="-p"
parser.add_argument("resource", type=str, choices=((ResourcePathType.GLOBAL_WORKSPACE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ResourcePathType.GLOBAL_LAYER.value)), help="resource to use")
parser.add_argument(ShortCode.CURRENT_DIMENSION.value, "--current-dimension", type=int, help="current layer dimension, ex. 8")
parser.add_argument(ShortCode.DEPTH_INDEX.value, "--depth-index", type=int, help="layer depth index, ex. 2")
parser.add_argument(ShortCode.PREVIOUS_DIMENSION.value, "--previous-dimension", type=int, help="current layer dimension, ex. 11")
# ***** WORKSPACE *****
# create
def createWorkspace(sourceFile):
    response = create(serverGlobalWorkspaceUrl,sourceFile)
    return response
# read single
def readSingleWorkspace(id,targetFile):
    URL=serverUrl+specificWorkspaceUrl(endpoint,id)
    readSingle(URL, targetFile)
    pass
# read all
def readAllWorkspace():
    response = readAll(serverGlobalWorkspaceUrl)
    return response
# update
def updateWorkspace(sourceFile):
    update(serverGlobalWorkspaceUrl, sourceFile)
    pass
# delete single
def deleteSingleWorkspace(id):
    URL=serverUrl+specificWorkspaceUrl(endpoint,id)
    delete(URL)
    pass
# delete all
def deleteAllWorkspace():
    delete(serverGlobalWorkspaceUrl)
    pass
# ***** PERCEPTRON *****
# create
def createPerceptron(sourceFile):
    response = create(serverGlobalPerceptronUrl,sourceFile)
    return response
# read single
def readSinglePerceptron(id,targetFile):
    URL=serverUrl+specificPerceptronUrl(endpoint,id)
    readSingle(URL, targetFile)
    pass
# read all
def readAllPerceptron(parentId):
    URL=serverUrl+globalPerceptronUrl(endpoint,parentId)
    response = readAll(URL)
    return response
# update
def updatePerceptron(sourceFile):
    update(serverGlobalPerceptronUrl, sourceFile)
    pass
# patch
def patchPerceptron(sourceFile):
    patch(serverGlobalPerceptronUrl,sourceFile)
    pass
# summarize
def summarizePerceptron(id,targetFile):
    URL=serverUrl+perceptronSummaryUrl(endpoint,id)
    summarize(URL,targetFile)
    pass
# execute
def executePerceptron(id,sourceFile,targetFile):
    URL=serverUrl+perceptronExecutionUrl(endpoint,id)
    execute(URL,sourceFile,targetFile)
    pass
# delete single
def deleteSinglePerceptron(id):
    URL=serverUrl+specificPerceptronUrl(endpoint,id)
    delete(URL)
    pass
# delete all
def deleteAllPerceptron(parentId):
    URL=serverUrl+globalPerceptronUrl(endpoint,parentId)
    delete(URL)
    pass
# ***** LAYER *****
# create
def createLayer(sourceFile,previousDimension,currentDimension):
    URL=serverUrl+globalLayerUrl(endpoint,previousDimension=previousDimension,currentDimension=currentDimension)
    create(URL, sourceFile, False)
    pass
# read single
def readSingleLayer(perceptronId,depthIndex,targetFile):
    URL=serverUrl+specificLayerUrl(endpoint,perceptronId,depthIndex)
    readSingle(URL, targetFile)
    pass
# read all
def readAllLayer(parentId):
    URL=serverUrl+globalLayerUrl(endpoint,perceptronId=parentId)
    response = readAll(URL)
    return response
# update
def updateLayer(sourceFile):
    update(serverGlobalLayerUrl, sourceFile)
    pass
# summarize
def summarizeLayer(perceptronId,depthIndex,targetFile):
    URL=serverUrl+layerSummaryUrl(endpoint,perceptronId,depthIndex)
    summarize(URL,targetFile)
    pass
# delete single
def deleteSingleLayer(perceptronId,depthIndex):
    URL=serverUrl+specificLayerUrl(endpoint,perceptronId,depthIndex)
    delete(URL)
    pass
# delete all
def deleteAllLayer(parentId):
    URL=serverUrl+globalLayerUrl(endpoint,perceptronId=parentId)
    delete(URL)
    pass
# parse arguments
def dispatchRequest(arguments):
    response = None
    action = arguments.action.lower()
    resource = arguments.resource.lower()
    # ***** WORKSPACE *****
    # create
    if action==CommonActionType.CREATE.value and resource==ResourcePathType.GLOBAL_WORKSPACE.value:
        response = createWorkspace(arguments.source_file)
    # read single
    elif action==CommonActionType.READ.value and resource==ResourcePathType.GLOBAL_WORKSPACE.value and arguments.id:
        readSingleWorkspace(arguments.id,arguments.target_file)
    # read all
    elif action==CommonActionType.READ.value and resource==ResourcePathType.GLOBAL_WORKSPACE.value:
        response = readAllWorkspace()
    # update
    elif action==CommonActionType.UPDATE.value and resource==ResourcePathType.GLOBAL_WORKSPACE.value:
        updateWorkspace(arguments.source_file)
    # delete single
    elif action==CommonActionType.DELETE.value and resource==ResourcePathType.GLOBAL_WORKSPACE.value and arguments.id:
        deleteSingleWorkspace(arguments.id)
    # delete all
    elif action==CommonActionType.DELETE.value and resource==ResourcePathType.GLOBAL_WORKSPACE.value:
        deleteAllWorkspace()
    # ***** PERCEPTRON *****
    # create
    elif action==CommonActionType.CREATE.value and resource==ResourcePathType.GLOBAL_PERCEPTRON.value:
        response = createPerceptron(arguments.source_file)
    # read single
    elif action==CommonActionType.READ.value and resource==ResourcePathType.GLOBAL_PERCEPTRON.value and arguments.id:
        readSinglePerceptron(arguments.id,arguments.target_file)
    # read all
    elif action==CommonActionType.READ.value and resource==ResourcePathType.GLOBAL_PERCEPTRON.value:
        response = readAllPerceptron(arguments.related_id)
    # update
    elif action==CommonActionType.UPDATE.value and resource==ResourcePathType.GLOBAL_PERCEPTRON.value:
        updatePerceptron(arguments.source_file)
    # patch
    elif action==CommonActionType.PATCH.value and resource==ResourcePathType.GLOBAL_PERCEPTRON.value:
        patchPerceptron(arguments.source_file)
    # summarize
    elif action==CommonActionType.SUMMARIZE.value and resource==ResourcePathType.GLOBAL_PERCEPTRON.value:
        summarizePerceptron(arguments.id,arguments.target_file)
    # execute
    elif action==CommonActionType.EXECUTE.value and resource==ResourcePathType.GLOBAL_PERCEPTRON.value:
        executePerceptron(arguments.id,arguments.source_file,arguments.target_file)
    # delete single
    elif action==CommonActionType.DELETE.value and resource==ResourcePathType.GLOBAL_PERCEPTRON.value and arguments.id:
        deleteSinglePerceptron(arguments.id)
    # delete all
    elif action==CommonActionType.DELETE.value and resource==ResourcePathType.GLOBAL_PERCEPTRON.value:
        deleteAllPerceptron(arguments.related_id)
    # ***** LAYER *****
    # create
    elif action==CommonActionType.CREATE.value and resource==ResourcePathType.GLOBAL_LAYER.value:
        createLayer(arguments.source_file,arguments.previous_dimension,arguments.current_dimension)
    # read single
    elif action==CommonActionType.READ.value and resource==ResourcePathType.GLOBAL_LAYER.value and arguments.id:
        readSingleLayer(arguments.id,arguments.depth_index,arguments.target_file)
    # read all
    elif action==CommonActionType.READ.value and resource==ResourcePathType.GLOBAL_LAYER.value:
        response = readAllLayer(arguments.related_id)
    # update
    elif action==CommonActionType.UPDATE.value and resource==ResourcePathType.GLOBAL_LAYER.value:
        updateLayer(arguments.source_file)
    # summarize
    elif action==CommonActionType.SUMMARIZE.value and resource==ResourcePathType.GLOBAL_LAYER.value:
        summarizeLayer(arguments.id,arguments.depth_index,arguments.target_file)
    # delete single
    elif action==CommonActionType.DELETE.value and resource==ResourcePathType.GLOBAL_LAYER.value and arguments.id:
        deleteSingleLayer(arguments.id,arguments.depth_index)
    # delete all
    elif action==CommonActionType.DELETE.value and resource==ResourcePathType.GLOBAL_LAYER.value:
        deleteAllLayer(arguments.related_id)
    # ***** error in command line positional arguments *****
    else:
        raiseArgumentsException()
    return response
# run program
if __name__ == "__main__":
    response = main(argv, parser, dispatchRequest, extraHelpFile)
    if response:
        print(response)
pass
