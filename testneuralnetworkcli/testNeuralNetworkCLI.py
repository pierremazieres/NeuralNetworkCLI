#!/usr/bin/env python3
# coding=utf-8
# import
from testneuralnetworkcommon.commonUtilities import randomWorkspace, CommonTest, insertRandomWorkspaces, randomPerceptron, randomLayer, insertRandomLayers, insertRandomPerceptrons
from neuralnetworkcli.neuralNetworkCLI import dispatchRequest, parser, ShortCode
from neuralnetworkcommon.service.service import ResourcePathType
from neuralnetworkcommon.cli.cli import ActionType, ShortCode as ShortCodeDefault
from pythoncommontools.objectUtil.POPO import loadFromDict
from json import loads
from neuralnetworkcommon.entity.workspace import Workspace
from neuralnetworkcommon.entity.layer.layer import Layer
from testpythoncommontools.cli.cli import checkRestClientDefaultError
from pythoncommontools.file.file import writeTemporaryFile, readFromFile
from pythoncommontools.cli.cli import main
from random import randint
from neuralnetworkcommon.database.layer import deleteAllByPerceptronId, selectByPerceptronIdAndDepthIndex
from random import choice
from string import ascii_letters
from neuralnetworkcommon.entity.perceptron.perceptron import Perceptron
from numpy.random import rand
# test neural network CLI
rawBadWorkspace = Workspace('')
del rawBadWorkspace.comments
badWorkspace = rawBadWorkspace.dumpToSimpleJson().encode()
badPerceptron = Perceptron('',None).dumpToSimpleJson().encode()
badLayer = Layer('',None).dumpToSimpleJson().encode()
class testNeuralNetworkCliWS(CommonTest):
    # test CRUD OK
    def testCrudOk(self):
        # initial workspace
        rawInitialWorkspace = randomWorkspace()
        tempFile = writeTemporaryFile(rawInitialWorkspace.dumpToSimpleJson().encode())
        # create workspace
        args = '',ActionType.CREATE.value,ResourcePathType.GLOBAL_WORKSPACE.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        workspaceId = main(args, parser, dispatchRequest)
        rawInitialWorkspace.id = workspaceId
        self.assertIsNotNone(workspaceId,"ERROR : workspace has no id")
        # read workspace
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_WORKSPACE.value,ShortCodeDefault.ID.value,str(workspaceId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedInsertedWorkspace = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(rawInitialWorkspace,fetchedInsertedWorkspace,"ERROR : inserted workspace does not match")
        # update workspace
        rawNewWorkspace = randomWorkspace(workspaceId)
        tempFile = writeTemporaryFile(rawNewWorkspace.dumpToSimpleJson().encode(),tempFile)
        args = '',ActionType.UPDATE.value,ResourcePathType.GLOBAL_WORKSPACE.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_WORKSPACE.value,ShortCodeDefault.ID.value,str(workspaceId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedUpdatedWorkspace = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertNotEqual(fetchedUpdatedWorkspace,fetchedInsertedWorkspace,"ERROR : workspace not updated")
        rawNewWorkspace.id = workspaceId
        self.assertEqual(fetchedUpdatedWorkspace,rawNewWorkspace,"ERROR : updated workspace does not match")
        # initial perceptron
        rawInitialPerceptron = randomPerceptron(workspaceId=workspaceId)
        tempFile = writeTemporaryFile(rawInitialPerceptron.dumpToSimpleJson().encode(),tempFile)
        # create perceptron
        args = '',ActionType.CREATE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        perceptronId = main(args, parser, dispatchRequest)
        rawInitialPerceptron.id = perceptronId
        self.assertIsNotNone(perceptronId,"ERROR : perceptron has no id")
        # read perceptron
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedInsertedPerceptron = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(rawInitialPerceptron,fetchedInsertedPerceptron,"ERROR : inserted perceptron does not match")
        # update perceptron
        rawNewPerceptron = randomPerceptron(perceptronId,workspaceId)
        tempFile = writeTemporaryFile(rawNewPerceptron.dumpToSimpleJson().encode(),tempFile)
        args = '',ActionType.UPDATE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedUpdatedPerceptron = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertNotEqual(fetchedUpdatedPerceptron,fetchedInsertedPerceptron,"ERROR : perceptron not updated")
        rawNewPerceptron.id = perceptronId
        self.assertEqual(fetchedUpdatedPerceptron,rawNewPerceptron,"ERROR : updated perceptron does not match")
        # initial layer
        depthIndex = 0
        rawInitialLayer = randomLayer(perceptronId,depthIndex)
        tempFile = writeTemporaryFile(rawInitialLayer.dumpToSimpleJson().encode(),tempFile)
        initialPreviousDimension = randint(2,5)
        initialCurrentDimension = randint(3,9)
        # create layer
        args = '',ActionType.CREATE.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name,ShortCode.PREVIOUS_DIMENSION.value,str(initialPreviousDimension),ShortCode.CURRENT_DIMENSION.value,str(initialCurrentDimension)
        main(args, parser, dispatchRequest)
        # read layer
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCode.DEPTH_INDEX.value,str(depthIndex),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedInsertedLayer = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(depthIndex,fetchedInsertedLayer.depthIndex,"ERROR : inserted layer depthIndex does not match")
        self.assertEqual(perceptronId,fetchedInsertedLayer.perceptronId,"ERROR : inserted layer perceptronId does not match")
        self.assertEqual(initialCurrentDimension,len(fetchedInsertedLayer.weights),"ERROR : inserted layer does not match")
        self.assertEqual(initialPreviousDimension,len(fetchedInsertedLayer.weights[0]),"ERROR : inserted layer does not match")
        self.assertEqual(initialCurrentDimension,len(fetchedInsertedLayer.biases),"ERROR : inserted layer biases does not match")
        # update layer
        updatedPreviousDimension = randint(12,15)
        updatedCurrentDimension = randint(13,19)
        rawNewLayer = randomLayer(perceptronId,depthIndex,updatedPreviousDimension,updatedCurrentDimension)
        tempFile = writeTemporaryFile(rawNewLayer.dumpToSimpleJson().encode(),tempFile)
        args = '',ActionType.UPDATE.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCode.DEPTH_INDEX.value,str(depthIndex),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedUpdatedLayer = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertNotEqual(fetchedUpdatedLayer,fetchedInsertedLayer,"ERROR : layer not updated")
        self.assertEqual(depthIndex,fetchedUpdatedLayer.depthIndex,"ERROR : updated layer depthIndex does not match")
        self.assertEqual(perceptronId,fetchedUpdatedLayer.perceptronId,"ERROR : updated layer perceptronId does not match")
        self.assertEqual(updatedCurrentDimension,len(fetchedUpdatedLayer.weights),"ERROR : updated layer does not match")
        self.assertEqual(updatedPreviousDimension,len(fetchedUpdatedLayer.weights[0]),"ERROR : updated layer does not match")
        self.assertEqual(updatedCurrentDimension,len(fetchedUpdatedLayer.biases),"ERROR : updated layer biases does not match")
        # summarize layer
        args = '',ActionType.SUMMARIZE.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCode.DEPTH_INDEX.value,str(depthIndex),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        layerSummary = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(layerSummary.perceptronId,perceptronId,"ERROR : layer summary perceptronId does not match")
        self.assertEqual(layerSummary.depthIndex,depthIndex,"ERROR : layer summary depthIndex does not match")
        self.assertEqual(layerSummary.weightsDimensions,[updatedCurrentDimension,updatedPreviousDimension],"ERROR : layer summary weightsDimensions does not match")
        self.assertEqual(layerSummary.biasesDimension,updatedCurrentDimension,"ERROR : layer summary initialCurrentDimension does not match")
        # delete layer
        args = '',ActionType.DELETE.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCode.DEPTH_INDEX.value,str(depthIndex)
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCode.DEPTH_INDEX.value,str(depthIndex),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        decompressedData = loads(readFromFile(tempFile.name))
        self.assertIsNone(decompressedData,"ERROR : layer not deleted")
        # summarize perceptron
        layersNumber = insertRandomLayers(perceptronId,True)
        CommonTest.connection.commit()
        args = '',ActionType.SUMMARIZE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        perceptronSummary = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(perceptronSummary.id,fetchedUpdatedPerceptron.id,"ERROR : perceptron summary id does not match")
        self.assertEqual(perceptronSummary.workspaceId,fetchedUpdatedPerceptron.workspaceId,"ERROR : perceptron summary workspaceId does not match")
        self.assertEqual(perceptronSummary.comments,fetchedUpdatedPerceptron.comments,"ERROR : perceptron summary comments does not match")
        self.assertEqual(perceptronSummary.layersNumber,layersNumber,"ERROR : perceptron summary layers does not match")
        # patch perceptron
        updatedComments = ''.join([choice(ascii_letters) for _ in range(50)])
        patchData = Perceptron(perceptronId,workspaceId, updatedComments)
        tempFile = writeTemporaryFile(patchData.dumpToSimpleJson().encode(),tempFile)
        args = '',ActionType.PATCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        fetchedPatchedPerceptron = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertEqual(fetchedPatchedPerceptron.workspaceId,fetchedUpdatedPerceptron.workspaceId,"ERROR : patched perceptron workspaceId not updated")
        self.assertNotEqual(fetchedPatchedPerceptron.comments,fetchedUpdatedPerceptron.comments,"ERROR : patched perceptron comments not updated")
        self.assertEqual(fetchedPatchedPerceptron.comments,updatedComments,"ERROR : patched perceptron comments does not match")
        # execute perceptron
        firstLayer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,perceptronId,0)
        inputVectorLength = len(firstLayer.weights[0])
        lastLayer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,perceptronId,layersNumber-1)
        expectedOutputVectorLength = len(lastLayer.weights)
        rawInputVector = [float(_) for _ in rand(inputVectorLength) ]
        tempFile = writeTemporaryFile(str(rawInputVector).encode(),tempFile)
        args = '',ActionType.EXECUTE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCodeDefault.SOURCE_FILE.value,tempFile.name,ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        outputVector = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        actualOutputVectorLength = len(outputVector)
        self.assertEqual(expectedOutputVectorLength,actualOutputVectorLength,"ERROR : perceptron outpout does not fit")
        # delete perceptron
        deleteAllByPerceptronId(CommonTest.cursor,perceptronId)
        CommonTest.connection.commit()
        args = '',ActionType.DELETE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.ID.value,str(perceptronId)
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.ID.value,str(perceptronId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        decompressedData = loads(readFromFile(tempFile.name))
        self.assertIsNone(decompressedData,"ERROR : perceptron not deleted")
        # delete workspace
        args = '',ActionType.DELETE.value,ResourcePathType.GLOBAL_WORKSPACE.value,ShortCodeDefault.ID.value,str(workspaceId)
        main(args, parser, dispatchRequest)
        args = '',ActionType.READ.value,ResourcePathType.GLOBAL_WORKSPACE.value,ShortCodeDefault.ID.value,str(workspaceId),ShortCodeDefault.TARGET_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        decompressedData = loadFromDict(loads(readFromFile(tempFile.name).decode()))
        self.assertIsNone(decompressedData,"ERROR : workspace not deleted")
        # close (& delete) temporary files
        tempFile.close()
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize data
        workspacesIds=insertRandomWorkspaces()
        perceptronIds=insertRandomPerceptrons()
        layerNumbers=insertRandomLayers(CommonTest.testPerceptronId)
        CommonTest.connection.commit()
        # select workspaces IDs
        readWorkspaceArgs = '',ActionType.READ.value,ResourcePathType.GLOBAL_WORKSPACE.value
        fetchedIds = main(readWorkspaceArgs, parser, dispatchRequest)
        self.assertTrue(workspacesIds.issubset(fetchedIds),"ERROR : workspace IDs selection does not match")
        # select perceptrons IDs
        readPerceptronArgs = '',ActionType.READ.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testWorkspaceId)
        fetchedIds = main(readPerceptronArgs, parser, dispatchRequest)
        self.assertTrue(perceptronIds.issubset(fetchedIds),"ERROR : perceptron IDs selection does not match")
        # select layers IDs
        readLayerArgs = '',ActionType.READ.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId)
        fetchedIds = main(readLayerArgs, parser, dispatchRequest)
        self.assertLessEqual(layerNumbers,len(fetchedIds),"ERROR : layer IDs selection does not match")
        # delete all layers
        args = '',ActionType.DELETE.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testPerceptronId)
        main(args, parser, dispatchRequest)
        remainingIds = main(readLayerArgs, parser, dispatchRequest)
        self.assertEqual(len(remainingIds),0,"ERROR : layer IDs deletion does not match")
        # delete all perceptrons
        args = '',ActionType.DELETE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.RELATED_ID.value,str(CommonTest.testWorkspaceId)
        main(args, parser, dispatchRequest)
        remainingIds = main(readPerceptronArgs, parser, dispatchRequest)
        self.assertEqual(len(remainingIds),0,"ERROR : perceptron IDs deletion does not match")
        ''' INFO : we do not check the "delete all" functionality in order to avoid :
         - delete other people workspace
         - error with existing workspace containing perceptrons or training sets'''
        # delete all workspaces
        args = ['',ActionType.DELETE.value, ResourcePathType.GLOBAL_WORKSPACE.value, ShortCodeDefault.ID.value]
        for workspacesId in workspacesIds:
            main(args+[str(workspacesId)], parser, dispatchRequest)
        remainingIds = main(readWorkspaceArgs, parser, dispatchRequest)
        self.assertNotIn(workspacesIds,remainingIds, "ERROR : workspace IDs deletion does not match")
        pass
    # test error
    ''' UNABLE TO REPRODUCE
    def testPostWorkspaceError(self):
        badTempFile = writeTemporaryFile(badWorkspace)
        args = '',ActionType.CREATE.value,ResourcePathType.GLOBAL_WORKSPACE.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    '''
    def testPutWorkspaceError(self):
        badTempFile = writeTemporaryFile(badWorkspace)
        args = '',ActionType.UPDATE.value,ResourcePathType.GLOBAL_WORKSPACE.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
        pass
    def testPostPerceptronError(self):
        badTempFile = writeTemporaryFile(badPerceptron)
        args = '',ActionType.CREATE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPutPerceptronError(self):
        badTempFile = writeTemporaryFile(badPerceptron)
        args = '',ActionType.UPDATE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPatchPerceptronError(self):
        badTempFile = writeTemporaryFile(badPerceptron)
        args = '',ActionType.PATCH.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPerceptronExecutionError(self):
        # generate perceptron
        insertRandomLayers(CommonTest.testPerceptronId, True)
        CommonTest.connection.commit()
        perceptron = randomPerceptron()
        tempFile = writeTemporaryFile(perceptron.dumpToSimpleJson().encode())
        args = '',ActionType.UPDATE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.SOURCE_FILE.value,tempFile.name
        main(args, parser, dispatchRequest)
        # execute perceptron
        firstLayer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,CommonTest.testPerceptronId,0)
        inputVectorLength = len(firstLayer.weights[0])+1
        rawInputVector = [float(_) for _ in rand(inputVectorLength) ]
        tempFile = writeTemporaryFile(str(rawInputVector).encode())
        args = '',ActionType.EXECUTE.value,ResourcePathType.GLOBAL_PERCEPTRON.value,ShortCodeDefault.ID.value,str(CommonTest.testPerceptronId),ShortCodeDefault.SOURCE_FILE.value,tempFile.name,ShortCodeDefault.TARGET_FILE.value,tempFile.name
        # check execution
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        # close (& delete) temporary files
        tempFile.close()
    def testPostLayerError(self):
        badTempFile = writeTemporaryFile(badLayer)
        args = '',ActionType.CREATE.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    def testPutLayerError(self):
        badTempFile = writeTemporaryFile(badLayer)
        args = '',ActionType.UPDATE.value,ResourcePathType.GLOBAL_LAYER.value,ShortCodeDefault.SOURCE_FILE.value,badTempFile.name
        checkRestClientDefaultError(self, main,args, parser, dispatchRequest)
        badTempFile.close()
    pass
pass
